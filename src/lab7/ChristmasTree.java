package lab7;

import java.io.Serializable;

public class ChristmasTree implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7780899596395414236L;
	
	private float price;
	protected float height;
	protected ChristmasTreeType type;
	protected transient float reducedPrice;
	
	public ChristmasTree(float price, float height, ChristmasTreeType type) {
		super();
		this.height = height;
		this.type = type;
		this.price = price;
		this.reducedPrice = price - price * .1f;
	}
	
	public ChristmasTree() {}
	
	public void setPrice (float price) {
		this.price = price;
		this.reducedPrice = price - price * .1f;
	}
	
	public float getPrice () {
		return price;
	}

	@Override
	public String toString() {
		return "ChristmasTree [price=" + price + " units, height=" + height + " cm, type=" + type + " sale price = " + reducedPrice +"]";
	}
}
