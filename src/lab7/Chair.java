package lab7;

import java.io.Serializable;

public class Chair implements Serializable {
	protected String producer, fabric;
	protected float price;
	public Chair(String producer, String fabric, float price) {
		super();
		this.producer = producer;
		this.fabric = fabric;
		this.price = price;
	}
	
	public Chair () {}
	
	@Override
	public String toString() {
		return "Chair [producer=" + producer + ", fabric=" + fabric + ", price=" + price + "]";
	}
}
