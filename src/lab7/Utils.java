package lab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public abstract class Utils {
	public static <T> void writeObjects (List<T> trees, String filename, String extension) {
        try {
			FileOutputStream f = new FileOutputStream(new File(filename + "." + extension));
	        ObjectOutputStream o = new ObjectOutputStream(f);

	        // Write objects to file
	        for (T tree : trees) {
	            o.writeObject(tree);
	        }
	        
            o.close();
            f.close();
	    } catch (IOException e) {
	        System.out.println("Error initializing stream");
	    }
	}

    @SuppressWarnings("unchecked")
    public static <T> List<T> readObjects (String filename, String extension)  {
        List<T> trees = new ArrayList<T>();
        try {
	        FileInputStream fi = new FileInputStream(new File(filename + "." + extension));
	        ObjectInputStream oi = new ObjectInputStream(fi);
	        
	        // Read objects
			T obj = (T)oi.readObject();
            do {
            	trees.add(obj);
    	        obj = (T)oi.readObject();
            } while (obj != null);

            oi.close();
            fi.close();
        } catch (FileNotFoundException e) {
//            System.out.println("File not found");
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
        
        return trees;
	}
	
	public static List<String> readStrings (String filename, String extension) {
		List<String> trees = new ArrayList<String>();
		try {
			Scanner s = new Scanner(new File(filename + "." + extension));
			while(s.hasNext()) {
				trees.add(s.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return trees;
	}
	
	protected static float generateRandomFloat (float times, int startFrom, int places) {
		String format = "";
		Random r = new Random();
		
		while (places > 0) {
			format += 0;
			places--;
		}
		
		DecimalFormat df = new DecimalFormat("##." + format);
		float number = r.nextFloat() * times + startFrom;
		return Float.parseFloat(df.format(number));
	}
}
