package lab7;

public class StudentException extends Exception {
	public StudentException (String msg) {
		super(msg);
	}
}
