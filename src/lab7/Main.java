package lab7;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class Main {
	private final static String DATA_PATH = "src/lab7/data/";

	public static void main (String ... args) throws IOException {
		lab7.Student s7 = new lab7.Student();
		lab6.Student s6 = new lab6.Student();
		
		System.out.println(s7.name);
//		System.out.println(s6.name);
		
		
//		ex1();
//		ex2();
//		ex3();
//		ex4();
//		
//		ex6();
//		ex7();
		
//		//Cars - Producer
//		Map<Producer, List<Car>> 
//		//Mercedes - C clase, B clase, S clase, etc..
//		Car, Order
//		Map<Order, Car> 
//		Car x, Orders
		
		List<String> str = new ArrayList<>();
		str.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		
		str.stream().filter(s -> s.length() % 2 != 0);

		List<ChristmasTree> trees = new ArrayList<>();
	}
	
	private static void ex1 () {
		int treesCount = 10;
		Random r = new Random();
		List<ChristmasTree> trees = new ArrayList<ChristmasTree>();
		for (int i = 0; i < treesCount; i++) {
			float price = Utils.generateRandomFloat(10, 5, 2);
			float height = Utils.generateRandomFloat(100, 150, 2);
			ChristmasTreeType type = ChristmasTreeType.values()[r.nextInt(2)];
			trees.add(new ChristmasTree(price, height, type));
		}
		System.out.println(trees);
		Utils.writeObjects(trees, DATA_PATH + "myTrees", "txt");
		System.out.println(Utils.readObjects(DATA_PATH + "myTrees", "txt"));
	}

    @SuppressWarnings("unused")
	private static void ex2 () {
		try {
//			LineNumberReader lnr = new LineNumberReader(new FileReader("F:\\Diverse\\Uni\\Assist\\Labs\\src\\lab7\\data\\ex2.txt"));
			LineNumberReader lnr = new LineNumberReader(new FileReader(DATA_PATH + "ex2.txt"));

			String line = lnr.readLine();
			do {
				int lineNumber = lnr.getLineNumber();
				
				if (lineNumber % 2 == 1 && Integer.parseInt(line) % 2 == 0)
					System.out.println(line);
			} while ((line = lnr.readLine()) != null);
			lnr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Scanner sc = new Scanner(new FileReader(DATA_PATH + "ex2.txt"));
			int sum = 0, count = 0;
			while (sc.hasNext()) {
				int number = sc.nextInt();
				sum += number;
				count++;
			}
			System.out.printf("Media aritmetica este %f, iar cea geometrica este %f\n", (float)sum/count, Math.pow(sum, (float)1/count));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
    private static void ex3 () {
		String emailRegex = "^(.+)@(.+)$"; 
		Pattern emailPattern = Pattern.compile(emailRegex);
		
		List<String> studentsTxt = Utils.readStrings(DATA_PATH + "ex3", "txt");
		List<Student> students = new ArrayList<Student>();
		for (String student : studentsTxt) {
			//Getting the List of information in a list while also trimming the data
			List<String> studentData = Arrays.asList(student.split(";")).stream().map(String::trim).collect(Collectors.toList());
			
			//Get and check the email
			String email = studentData.get(0);
			Matcher matcher = emailPattern.matcher(email);
			if (!matcher.matches()) {
				System.out.println("Invalid email at line " + (studentsTxt.indexOf(student)+1));
				return;
			}
			//Get the rest of the fields from the file
			String name = studentData.get(1);
			//All the remaining String will go into the skills array
			List<String> skills = studentData.subList(2, studentData.size());
			Student st = new Student(email, name, skills);
			students.add(st);
		}
		System.out.println(students);
		//Displaying all the skills inside a map with <Skill, Emails> as pair
		Map<String, List<String>> skills = new HashMap<String, List<String>>();
		for (Student student : students) {
			for (String skill : student.skills) {
				if (skills.get(skill) == null) {
					skills.put(skill, new ArrayList<String>());
				}
				skills.get(skill).add(student.email);
			}
		}
		System.out.println(skills);
    }
    
    private static void ex4 () {
    	//Makes the Scanner listen to the console
    	@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
    	//Gets the entry from the console
    	System.out.print("Enter the relative path: ");
    	String path = sc.next();
    	//Gets the path from the relative path("Root of the project")
    	File folder = new File(path);
    	System.out.println(folder.getAbsolutePath());
    	if (folder.isDirectory()) {
    		for (File file : folder.listFiles()) {
    			if (file.isDirectory()) {
    				System.out.println("Folder " + file.getName() + " contains " + file.listFiles().length + " files");
    			} else {
    				System.out.println("File " + file.getName() + " found at " + file.getPath() + " of size in bytes " + file.length());
    			}
    		}
    	}
    }

    private static void ex6 () {
    	//Gets the csv file as a string array based on the rows
    	List<String> chairsTxt = Utils.readStrings(DATA_PATH + "ex6", "csv");
    	List<Chair> chairs = new ArrayList<Chair>();
    	for (String ch : chairsTxt) {
    		//Split by the comma
    		List<String> chAttributes = Arrays.asList(ch.split(","));
    		String producer = chAttributes.get(0).substring(1);
    		String fabric = chAttributes.get(1);
    		float price = Float.parseFloat(chAttributes.get(2).substring(0, chAttributes.get(2).length()-1));
    		
    		chairs.add(new Chair(producer, fabric, price));
    	}
    	System.out.println(chairs);
    	chairs.sort(new Comparator<Chair>() {
			@Override
			public int compare(Chair o1, Chair o2) {
				return Float.compare(o1.price, o2.price);
			}
		});
    	System.out.println(chairs);
    	Map<String, List<Chair>> producers = new HashMap<String, List<Chair>>();
    	for (Chair chair : chairs) {
			if (producers.get(chair.producer) == null) {
				producers.put(chair.producer, new ArrayList<Chair>());
			}
			producers.get(chair.producer).add(chair);
		}
    	System.out.println(producers);
    	for (Entry<String, List<Chair>> entry : producers.entrySet()) {
    		Utils.writeObjects(entry.getValue(), DATA_PATH + entry.getKey(), "txt");
    	}
    }
    
    @SuppressWarnings("resource")
	private static void ex7 () throws IOException {
    	RandomAccessFile raf = null;
    	try {
			raf = new RandomAccessFile(DATA_PATH + "ex7.txt", "rw");
		} catch (FileNotFoundException e) {
    		System.err.println("File Not found.");
    		return;
		}
    	
    	Random r = new Random();
    	//Write data
    	for (int i = 0; i < 10; i++) {
			raf.writeShort(i%2 == 0 ? -1*r.nextInt(100) : r.nextInt(100));
    	}
    	
    	//Read data
    	int [] data = new int[10];
    	try {
    		raf.seek(0);
    		int i = 0;
    		data[i] = raf.readShort();
			while (data[i] != -1) {
				System.out.println(data[i]);
				i++;
				data[i] = raf.readShort();
			}
    	} catch (EOFException eof) {
    		
    	}
    	
    	for (int i = 0; i < 10; i++) {
    		data[i] = Math.abs(data[i]);
    		System.out.println(data[i]);
    	}
    }
}
