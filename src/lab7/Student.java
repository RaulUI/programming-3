package lab7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Student implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7638356776254449113L;
	protected String email;
	protected String name;
	protected List<String> skills = new ArrayList<String>();
	
	public Student(String email, String name, List<String> skills) {
		super();
		this.email = email;
		this.name = name;
		this.skills = skills.subList(1, 2);
	}
	
	public Student() {}
	
	@Override
	public String toString() {
		return "Student [email=" + email + ", name=" + name + ", skills=" + skills + "]\n";
	}
}
