package lab3;

public class Circle extends Figure {

	private float r;
	
	public Circle() {
		this.type = FigureType.Circle;
	}
	
	public Circle(float r) {
		super();
		this.r = r;
		this.type = FigureType.Circle;
	}

	@Override
	public double area() {
		return Math.PI * Math.pow(r, 2);
	}

	@Override
	public double perimeter() {
		return 2 * Math.PI * r;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	@Override
	public String toString() {
		return "[r=" + r + ", filled=" + filled + "]";
	}
}
