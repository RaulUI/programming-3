package lab3;

public interface FigureComputations {
	public double area();
	public double perimeter();
}