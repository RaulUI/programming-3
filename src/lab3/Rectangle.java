package lab3;

public class Rectangle extends Figure {
	private float width;
	private float length;
	
	public Rectangle() {
		super();
		this.type = FigureType.Rectangle;
	}
	
	public Rectangle(float width, float length, boolean filled) {
		super();
		this.width = width;
		this.length = length;
		this.filled = filled;
		this.type = FigureType.Rectangle;
	}

	@Override
	public double area() {
		return width*length;
	}
	
	@Override
	public double perimeter() {
		return 2*width + 2*length;
	}
	
	public float getWidth() {
		return width;
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	
	public float getLength() {
		return length;
	}
	
	public void setLength(float length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "[width=" + width + ", length=" + length + ", filled=" + filled + "]";
	}
}
