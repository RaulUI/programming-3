package lab3;

public enum FigureType {
	Circle(1),
	Rectangle(2),
	Trapezium(3);
	
	int id;
	
	FigureType (int id) {
		this.id = id;
	}
}