package lab3;

public abstract class Figure implements FigureComputations {
	protected boolean filled;
	protected FigureType type;

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	@Override
	public String toString() {
		return "Figure [filled=" + filled + "]";
	}
}
