package lab3;

import java.util.ArrayList;
import java.util.List;

public class lab {
	
	public static void main(String ... args) {
		
		List<Figure> figures = new ArrayList<Figure>();
		figures.add(new Circle(3));
		figures.add(new Circle(5.5f));
		figures.add(new Circle(1.3f));
		figures.add(new Rectangle(2, 3, true));
		figures.add(new Rectangle(5, 2, true));
		figures.add(new Rectangle(9, 1, true));
		figures.add(new Rectangle(5, 8, true));
		
		for (Figure fig : figures) {
			if (fig instanceof Circle) {
//			if (fig.type == FigureType.Circle) {
				System.out.printf("Area of %s %s is %f\n", fig.type, fig, fig.area());
			}
			if (fig instanceof Rectangle) {
				System.out.printf("Perimeter of %s %s is %f\n", fig.type, fig, fig.perimeter());
			}
		}
	}
}
