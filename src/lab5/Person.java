package lab5;

import java.util.List;

public class Person {
	protected String firstName, lastName;
	int yearOfBirth;
	List<String> lasteAtendeeStudies;
	
	public Person () {}
	
	public Person(String firstName, String lastName, int yearOfBirth, List<String> lasteAtendeeStudies) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.yearOfBirth = yearOfBirth;
		this.lasteAtendeeStudies = lasteAtendeeStudies;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getYearOfBirth() {
		return yearOfBirth;
	}
	public void setYearOfBirth(int yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}
	public List<String> getLasteAtendeeStudies() {
		return lasteAtendeeStudies;
	}
	public void setLasteAtendeeStudies(List<String> lasteAtendeeStudies) {
		this.lasteAtendeeStudies = lasteAtendeeStudies;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", yearOfBirth=" + yearOfBirth + "]";
	}
	
	@Override
	public boolean equals(Object p2) {
		if (p2 instanceof Person) {
			return lastName.compareTo(((Person)p2).lastName) == 0 ? true : false;
		} else {
			return false;
		}
	}
}
