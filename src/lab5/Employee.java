package lab5;

public class Employee extends Person {
	protected int salary;

	public Employee(int salary) {
		super();
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [salary=" + salary + ", firstName=" + firstName + ", lastName=" + lastName + ", yearOfBirth="
				+ yearOfBirth + ", lasteAtendeeStudies=" + lasteAtendeeStudies + "]";
	}
}
