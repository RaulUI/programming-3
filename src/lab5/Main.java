package lab5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		ex1 ();
		ex2 (args);
	}

	private static void ex1() {
		List<Person> personsList = new ArrayList<Person>();
		Person p1 = new Person("F Name 3", "L Name 3", 1992, Arrays.asList("Math", "Science", "Arts"));
		Person p2 = new Person("F Name 2", "L Name 2", 1991, Arrays.asList("Music", "Law"));
		personsList.add(p1);
		personsList.add(p2);
		
		@SuppressWarnings("unused")
		Pair<String> str = new Pair<String>("A", "B");

		//C)
		List<Pair<Person>> persons = new ArrayList<Pair<Person>>();
		persons.add(
				new Pair<Person>(
						new Person ("F Name 3", "L Name 3", 1992, Arrays.asList("Math", "Science", "Arts")),
						new Person ("F Name 2", "L Name 2", 1991, Arrays.asList("Music", "Law"))));
		persons.add(
				new Pair<Person>(
						new Person ("F Name 1", "L Name 1", 1990, Arrays.asList("English", "Databases", "Arts")),
						new Person ("F Name 4", "L Name 4", 1994, Arrays.asList("Biology", "Medicine", "Chemistry"))));
		persons.add(
				new Pair<Person>(
						new Person ("F Name 5", "L Name 5", 1995, Arrays.asList("Astronmoy", "Geography", "Chemistry, Physics")),
						new Person ("F Name 6", "L Name 6", 1996, Arrays.asList("Math", "Science", "Philosophy"))));
		
		// D)
		persons.sort(new Comparator<Pair<Person>>() {
			@Override
			public int compare(Pair<Person> o1, Pair<Person> o2) {
				return o1.x.lastName.compareTo(o2.x.lastName);
			}
		});
		
		// E)
		Map<String, List<Person>> studies = new HashMap<String, List<Person>>();
		for (Pair<Person> personPair : persons) {
			for (String study : personPair.getX().getLasteAtendeeStudies()) {
				if (studies.get(study) == null || studies.get(study).isEmpty()) {
					studies.put(study, new ArrayList<Person>());
				}
				studies.get(study).add(personPair.getX());
			}
			for (String study : personPair.getY().getLasteAtendeeStudies()) {
				if (studies.get(study) == null || studies.get(study).isEmpty()) {
					studies.put(study, new ArrayList<Person>());
				}
				studies.get(study).add(personPair.getY());
			}
		}
		
		System.out.println(persons);
		System.out.println(studies);
		
		// G)
		display(personsList);
		
		// I)
		System.out.println(persons.stream().min(Comparator.comparing(ps -> getMinAge((Pair<Person>) ps).yearOfBirth)));
	}
	
	private static void ex2(String ... args) {
		List<Flower> flowers = new ArrayList<Flower>();
		flowers.add(new Flower("Lily", Species.Dahlia, 13));
		flowers.add(new Flower("Rose", Species.Rosa, 36));
		flowers.add(new Flower("Daisy", Species.Acacia, 8));
		flowers.add(new Flower("Tulip", Species.Rosa, 25));
		flowers.add(new Flower("Daffodil", Species.Passiflora, 5));
		flowers.add(new Flower("Lavender", Species.Viola, 18));
		
		// A)
		System.out.println(
				flowers
				.stream()
				.mapToInt(Flower::getPrice)
				.average()
				.getAsDouble()
				);
		
		// B)
		String species = args[0];
		System.out.println(
				flowers
				.stream()
				.filter(flower -> flower.getSpecies().getName().equals(species))
				.collect(Collectors.toList())
			);
		
		// C)
		String name = "L";
		System.out.println(
				flowers
				.stream()
				.filter(flower -> flower.getName().startsWith(name))
				.collect(Collectors.toList())
			);
	}
	
	public static <T> void display(List<T> list) {
		System.out.println("Our own display method");
		if (list.isEmpty()) {
			System.err.println("We won't display an empty list in here!");
			return;
		}
		if (list.get(0) instanceof Pair) {
			System.err.println("We won't display Pairs in here!");
			return;
		}
		for (Object elem : list) {
			System.out.println(elem);
		}
	}
	
	public static Person getMinAge(Pair<Person> ps) {
		if (ps.x.yearOfBirth < ps.y.yearOfBirth)
			return ps.x;
		else
			return ps.y;
	}
}
