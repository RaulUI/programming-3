package lab5;

public class Student extends Person {
	protected int noTaxSemesters;

	public Student(int noTaxSemesters) {
		super();
		this.noTaxSemesters = noTaxSemesters;
	}
	
	public int getNoTaxSemesters() {
		return noTaxSemesters;
	}

	public void setNoTaxSemesters(int noTaxSemesters) {
		this.noTaxSemesters = noTaxSemesters;
	}

	@Override
	public String toString() {
		return "Employee [noTaxSemesters=" + noTaxSemesters + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", yearOfBirth=" + yearOfBirth + ", lasteAtendeeStudies=" + lasteAtendeeStudies + "]";
	}
}
