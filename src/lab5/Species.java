package lab5;

public enum Species {
	Passiflora,
	Acacia,
	Rosa,
	Viola,
	Dahlia;
	
	public String getName() {
		return name();
	}
}
