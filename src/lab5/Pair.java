package lab5;

public class Pair<T> {
	protected T x;
	protected T y;

	public Pair () {
		
	}
	
	public Pair(T x, T y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public void interchange () {
		T aux = this.x;
		this.x = this.y;
		this.y = aux;
	}
	
	public T getX() {
		return x;
	}

	public void setX(T x) {
		this.x = x;
	}

	public T getY() {
		return y;
	}

	public void setY(T y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "Pair [x=" + x + ", y=" + y + "]\n";
	}
	
}
