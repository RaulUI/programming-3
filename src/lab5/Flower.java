package lab5;

public class Flower {
	protected String name;
	protected Species species;
	protected int price;
	public Flower() {}
	public Flower(String name, Species species, int price) {
		super();
		this.name = name;
		this.species = species;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Species getSpecies() {
		return species;
	}
	public void setSpecies(Species species) {
		this.species = species;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Flower [name=" + name + ", species=" + species + ", price=" + price + "]";
	}
}
