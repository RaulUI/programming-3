package lab6;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Student {
	protected String name;
	protected int year;
	protected Map<Subject, List<Integer>> subjects = new HashMap<Subject, List<Integer>>();
	
	private double mean;
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	
	public Student () {}
	
	public Student(String name, int year) throws StudentException {
		super();
		this.name = name;
		if (year < 0 || year > 12) {
			throw new StudentException("The year of the student is not from the interval [0, 12].");
		}
		this.year = year;
	}
	
	public Student(String name, int year, Map<Subject, List<Integer>> subjects) throws StudentException {
		super();
		this.name = name;
		if (year < 0 || year > 12) {
			throw new StudentException("The year of the student is not from the interval [0, 12].");
		}
		this.year = year;
		this.subjects = subjects;
	}
	
	public void addGrade(Subject key, Integer value) throws StudentException {
		if (value < 1 || value > 10) {
			throw new StudentException("Grade interval wrong.");
		}
		
		if (this.subjects.get(key) == null) {
			this.subjects.put(key, new ArrayList<Integer>());
		}
		this.subjects.get(key).add(value);
	}
	
	public double getMean() {
		if (this.subjects.isEmpty()) {
//			throw new StudentException("The student does not have any grades.");
			System.err.println("The student does not have any grades.");
		}
		
		mean = 0;
		subjects.values()
					.forEach(
						grades -> mean += grades
											.stream()
											.mapToInt(a -> a)
											.average()
											.getAsDouble()
						);
		
		return mean / subjects.size();
	}
	
	public double getMean(Subject subject) throws StudentException {
		if (this.subjects.isEmpty()) {
			throw new StudentException("The student does not have any grades.");
		}
		if (this.subjects.get(subject).isEmpty()) {
			throw new StudentException("The student does not have any grades for subject " + subject);
		}
		
		return subjects
				.get(subject)
				.stream()
				.mapToInt(a -> a)
				.average()
				.getAsDouble();
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) throws StudentException {
		if (year < 0 || year > 12) {
			throw new StudentException("The year of the student is not from the interval [0, 12].");
		}
		this.year = year;
	}
	public Map<Subject, List<Integer>> getSubjects() {
		return subjects;
	}
	public void setSubjects(Map<Subject, List<Integer>> subjects) {
		this.subjects = subjects;
	}
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", year=" + year + ", subjects=" + subjects + ", mean=" + df2.format(this.getMean()) + "]\n";
	}
}
