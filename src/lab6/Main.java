package lab6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) throws StudentException {
		List<Student> studenti = new ArrayList<Student>();

		studenti.add(new Student("Andrei", 5, generateRandomSubjects()));
		studenti.add(new Student("Ioan", 11, generateRandomSubjects()));
		studenti.add(new Student("George", 10, generateRandomSubjects()));
		studenti.add(new Student("Anita", 4, generateRandomSubjects()));
		studenti.add(new Student("Cristina", 11, generateRandomSubjects()));
		studenti.add(new Student("Mihai", 8, generateRandomSubjects()));
		studenti.add(new Student("Horatiu", 12, generateRandomSubjects()));		
		
		System.out.println(studenti);
		
		studenti.sort(new Comparator<Student>() {
			@Override
			public int compare(Student o1, Student o2) {
				return Double.compare(o1.getMean(), o2.getMean());
			}
		});
		
		System.out.println("\nSorted by mean list");
		System.out.println(studenti);
		
		System.out.println("\nOnly passing students");
		List<Student> passing = studenti
									.stream()
									.filter(student -> student.getMean() >= 5)
									.collect(Collectors.toList());
		System.out.println(passing);
		
		Student highestMean = studenti
									.stream()
									.max(Comparator.comparingDouble(Student::getMean))
									.get();
		Student lowestMean = studenti
									.stream()
									.min(Comparator.comparingDouble(Student::getMean))
									.get();
		
		System.out.printf("\nStudent with the highest mean is %s[%f], while the student with the lowest mean is %s[%f]",
				highestMean.getName(), highestMean.getMean(),
				lowestMean.getName(), lowestMean.getMean());
		
		Student mostSubjects = studenti
									.stream()
									.max(new Comparator<Student>() {
										@Override
										public int compare(Student o1, Student o2) {
											return Integer.compare(o1.getSubjects().size(), o2.getSubjects().size());
										}
									})
									.get();
		System.out.println("\nStudent who attended the most subjects is " + mostSubjects);
		
		System.out.println("\nFirst 3 students");
		Collections.reverse(studenti);
		System.out.println(studenti.stream().limit(3).collect(Collectors.toList()));
		
		System.out.println("\nMark Reports");
		Map<Integer, Integer> report = new HashMap<Integer, Integer>();
		for (Student student : studenti) {
			for (List<Integer> grades : student.getSubjects().values()) {
				for (Integer grade : grades) {
					if (report.containsKey(grade)) {
						report.put(grade, report.get(grade) + 1);
					} else {
						report.put(grade, 1);
					}
				}
			}
		}
		
		for (Entry<Integer, Integer> frequency : report.entrySet()) {
			System.out.printf("Grade %d has the frequency %d\n", frequency.getKey(), frequency.getValue());
		}
	}
	
	public static Map<Subject, List<Integer>> generateRandomSubjects() {
		Map<Subject, List<Integer>> grades = new HashMap<Subject, List<Integer>>();
		Random r = new Random();
		
		int subjectsCount = r.nextInt(4) + 2;
		for (int i = 0; i < subjectsCount; i++) {
			int gradesCount = r.nextInt(3) + 1;
			List<Subject> subjects = Arrays.asList(Subject.values());
			Subject subject = subjects.get(r.nextInt(subjects.size()-1));
			
			grades.put(subject, new ArrayList<Integer>());		
			
			//Gaussian distribution of the grades for more accurate data
			for (int j = 0; j < gradesCount; j++) {
				int grade = (int)(r.nextGaussian()*1.5f + 6);
				if (grade < 1) grade = 1;
				if (grade > 10) grade = 10;
				grades.get(subject).add(grade);
			}
			
			//Plain random generator
//			for (int j = 0; j < gradesCount; j++) {
//				grades.get(subject).add(r.nextInt(7)+3);
//			}
		}
		return grades;
	}

}
