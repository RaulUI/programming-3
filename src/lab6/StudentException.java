package lab6;

public class StudentException extends Exception {
	private static final long serialVersionUID = 3548804109327382174L;

	public StudentException () {
		super("Error at an object of type Student.");
	}
	
	public StudentException (String exception) {
		super(exception);
	}
}
