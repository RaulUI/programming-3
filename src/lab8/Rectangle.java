package lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;

public class Rectangle extends Figure {
	int width, height;
	
	public Rectangle(Color borderColor, boolean fill) {
		super(borderColor, fill);
        width = new Random().nextInt(50)+50;
        height = new Random().nextInt(50)+50;
	}

	@Override
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		g2d.setColor(this.color);
        if (this.fill) {
            g2d.fillRect(x, y, width, height);
        } else {
        	g2d.drawRect(x, y, width, height);
        }
	}
}
