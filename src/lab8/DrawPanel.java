package lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class DrawPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2025009841282459142L;

	public DrawPanel() {
	    setBackground(Color.DARK_GRAY);
		setBorder(new LineBorder(Color.LIGHT_GRAY));
		setBounds(864, 11, -434, 539);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

//        for (Figure f : DrawingBoard.figures) {
//        	if (f instanceof Circle) {
//                Ellipse2D.Double circle = new Ellipse2D.Double(100, 100, 50, 50);
//                g2.setColor(f.color);
//                if (f.fill)
//                	g2.fill(circle);
//                else
//                	g2.draw(circle);
//        	} else if (f instanceof Rectangle) {
//        		//....
//        	}
//        }
        if (DrawingBoard.figures == null || DrawingBoard.figures.isEmpty()) {
        	return;
        }
        for (Figure f : DrawingBoard.figures) {
        	f.draw(g2);
        }
	}
}
