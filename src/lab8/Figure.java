package lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public abstract class Figure {
	Color color;
	boolean fill;
	int x, y;
	
	public Figure() {
		super();
	}
	
	public Figure(int x, int y, Color borderColor, boolean fill) {
		super();
		this.x = x;
		this.y = y;
		this.color = borderColor;
		this.fill = fill;
	}

	public Figure(Color borderColor, boolean fill) {
		super();
	    x = new Random().nextInt(500);
	    y = new Random().nextInt(500);
		this.color = borderColor;
		this.fill = fill;
	}
	
	public abstract void draw(Graphics g);
}
