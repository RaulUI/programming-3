package lab8;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lab7.Utils;
import javax.swing.JList;

public class Main {
	private static JList<Song> listSongs;
	private static JTextField tfTitle;
	private static JTextField tfSinger;
	private static JSpinner spLength;
	
	private static List<Song> songs = generateSongs();
	private static int currentSong = 0;
	
	private static final String SONGS_FILE = "src/lab7/data/songs";

	public static void main(String[] args) {
		createUI();
		selectSong(currentSong);
	}
	
	private static List<Song> generateSongs () {
		List<Song> songs = new ArrayList<Song>();
		songs.add(new Song("Title 1", "Singer 1", 1));
		songs.add(new Song("Title 2", "Singer 2", 2));
		songs.add(new Song("Title 3", "Singer 3", 3));
		songs.add(new Song("Title 4", "Singer 4", 4));
		songs.add(new Song("Title 5", "Singer 5", 5));
		return songs;
	}
	
	private static void selectSong(int id) {
		if (id >= songs.size() || id < 0) {
			//Handle the count of the items
			if (id < 0) currentSong++;
			else if (id >= songs.size()) currentSong--;
			System.out.println("List is over.");
			return;
		}
		Song song = songs.get(id);
		tfTitle.setText(song.title);
		tfSinger.setText(song.singer);
		spLength.setValue(song.length);

		DefaultListModel<Song> model = new DefaultListModel<>();
		model.addAll(songs);
		listSongs.setModel(model);
		listSongs.setSelectedIndex(id);
	}
	
	private static void createUI () {
		songs = Utils.readObjects(SONGS_FILE, "txt");
		
		JFrame f=new JFrame("Winamp");
		          
		f.setSize(400,500);//400 width and 500 height  
		f.getContentPane().setLayout(null);//using no layout managers  
		
		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				songs.remove(currentSong);
				selectSong(--currentSong);
			}
		});
		btnNewButton.setBounds(285, 419, 89, 23);
		f.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				songs.add(new Song(tfTitle.getText(), tfSinger.getText(), (int)spLength.getValue()));
				selectSong(currentSong=songs.size()-1);
			}
		});
		btnNewButton_1.setBounds(10, 419, 89, 23);
		f.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Last");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSong(currentSong=songs.size()-1);
			}
		});
		btnNewButton_2.setBounds(285, 363, 89, 23);
		f.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Next");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSong(++currentSong);
			}
		});
		btnNewButton_3.setBounds(202, 363, 73, 23);
		f.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("Previous");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSong(--currentSong);
			}
		});
		btnNewButton_4.setBounds(109, 363, 73, 23);
		f.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("First");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectSong(currentSong=0);
			}
		});
		btnNewButton_5.setBounds(10, 363, 89, 23);
		f.getContentPane().add(btnNewButton_5);
		
		JLabel lblNewLabel = new JLabel("Title:");
		lblNewLabel.setBounds(10, 11, 46, 14);
		f.getContentPane().add(lblNewLabel);
		
		tfTitle = new JTextField();
		tfTitle.setBounds(66, 8, 308, 20);
		f.getContentPane().add(tfTitle);
		tfTitle.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Singer:");
		lblNewLabel_1.setBounds(10, 46, 46, 14);
		f.getContentPane().add(lblNewLabel_1);
		
		tfSinger = new JTextField();
		tfSinger.setBounds(66, 43, 308, 20);
		f.getContentPane().add(tfSinger);
		tfSinger.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Length:");
		lblNewLabel_2.setBounds(10, 82, 46, 14);
		f.getContentPane().add(lblNewLabel_2);
		
		spLength = new JSpinner();
		spLength.setBounds(66, 79, 308, 20);
		f.getContentPane().add(spLength);
		
		listSongs = new JList<Song>();
		listSongs.setBounds(10, 120, 364, 225);
		f.getContentPane().add(listSongs);
		f.setVisible(true);//making the frame visible
		listSongs.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting())
					selectSong(currentSong=e.getFirstIndex());
			}
		});
		
		f.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
		        Utils.writeObjects(songs, SONGS_FILE, "txt");
		    }
		});
	}
}
