package lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.util.Random;

public class Star extends Figure{
	
	public Star(Color borderColor, boolean fill) {
		super(borderColor, fill);
	}
	
	@Override
	public void draw(Graphics g) {
		int size = new Random().nextInt(1)+3;
		int [] xPoints = {size*9, size*15, 0, size*18, size*3};
	    int [] yPoints = {0, size*18, size*6, size*6, size*18};

        Graphics2D g2d = (Graphics2D) g;
        GeneralPath star = new GeneralPath();

        star.moveTo(xPoints[0] + x, yPoints[0] + y);
        for (int i = 1; i < xPoints.length; i++) {
            star.lineTo(xPoints[i] + x, yPoints[i] + y);
        }
        star.closePath();
        
        g2d.setColor(this.color);
        if (this.fill) {
            g2d.fill(star);
        } else {
        	g2d.draw(star);
        }
	}
}
