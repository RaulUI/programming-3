package lab8;

import java.io.Serializable;
import java.sql.Date;

public class Song implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1549809613678377495L;
	protected String title;
	protected String singer;
	protected int length;
	
	public Song(String title, String singer, int length) {
		super();
		this.title = title;
		this.singer = singer;
		this.length = length;
	}
	
	public String secondsToMinutes (int seconds) {
		int minutes = seconds / 60;
		int secs = seconds % 60;
		return String.format("%s:%s", minutes, secs);
	}

	@Override
	public String toString() {
		return singer + " - " + title + " (" + secondsToMinutes(length) + ")";
	}
}
