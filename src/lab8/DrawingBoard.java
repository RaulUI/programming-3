package lab8;

import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.awt.event.ActionEvent;

public class DrawingBoard extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String [] COLORS = {"Red", "Blue", "Green", "Yellow"};
	
	JPanel drawingPanel = new DrawPanel();
	static List<Figure> figures = new ArrayList<Figure>();
	ButtonGroup group = new ButtonGroup();
	JToggleButton tglFill = new JToggleButton("Fill");
	JComboBox cbColor = new JComboBox(COLORS);
	
	public static void main(String[] args) {
		new DrawingBoard();
	}

	public DrawingBoard() {
		
		setSize(900,600);
		getContentPane().setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new LineBorder(Color.LIGHT_GRAY));
		getContentPane().add(panel_1);
		
		JLabel lblNewLabel = new JLabel("Add new figure");
		lblNewLabel.setBounds(50, 11, 121, 14);
		panel_1.add(lblNewLabel);
		
		tglFill.setBounds(39, 201, 121, 23);
		panel_1.add(tglFill);
		
		JRadioButton rbRectangle = new JRadioButton("Rectangle");
		rbRectangle.setSelected(true);
		rbRectangle.setBounds(6, 43, 96, 23);
		panel_1.add(rbRectangle);
		JRadioButton rbCircle = new JRadioButton("Circle");
		rbCircle.setBounds(100, 77, 87, 23);
		panel_1.add(rbCircle);
		JRadioButton rbStar = new JRadioButton("Star");
		rbStar.setBounds(100, 43, 87, 23);
		panel_1.add(rbStar);
		JRadioButton rbSmiley = new JRadioButton("Smiley");
		rbSmiley.setBounds(6, 77, 88, 23);
		panel_1.add(rbSmiley);
		group.add(rbRectangle);
		group.add(rbCircle);
		group.add(rbStar);
		group.add(rbSmiley);
		
		cbColor.setBounds(36, 154, 121, 22);
		panel_1.add(cbColor);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String type = getSelectedButtonText(group);
				String color = (String) cbColor.getSelectedItem();
				addFigure(type, color, tglFill.isSelected());
				//Uncomment the below line to save the panel as a screenshot
//				takeSnapShot(drawingPanel);
			}
		});
		btnNewButton.setBounds(52, 265, 89, 23);
		panel_1.add(btnNewButton);
		getContentPane().add(drawingPanel);
        setVisible(true);
	}

	private void addFigure(String type, String col, boolean fill) {
		Figure f = null;
		Color color;
		try {
		    Field field = Class.forName("java.awt.Color").getField(col.toLowerCase());
		    color = (Color)field.get(null);
		} catch (Exception e) {
		    return;
		}
		switch (type) {
		case "Rectangle": f = new Rectangle(color, fill); break;
		case "Star": f = new Star(color, fill); break;
		case "Circle": f = new Circle(color, fill); break;
		case "Smiley": f = new Smiley(color, fill); break;
		}
		if (f == null) return;
		figures.add(f);
		drawingPanel.repaint();
	}
	
	public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }
        
        return null;
    }
	
	void takeSnapShot(JPanel panel ){
		BufferedImage bufImage = new BufferedImage(panel.getSize().width, panel.getSize().height,BufferedImage.TYPE_INT_RGB);
		panel.paint(bufImage.createGraphics());
		File imageFile = new File("F://figures.jpg");
	    try{
	        imageFile.createNewFile();
	        ImageIO.write(bufImage, "jpeg", imageFile);
	    }catch(Exception ex){
	    }
	}
}
