package lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.Random;

public class Smiley extends Figure {
	int size;

	public Smiley(Color borderColor, boolean fill) {
		super(borderColor, fill);
        size = new Random().nextInt(50)+50;
	}
	
	public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        Ellipse2D.Double circle = new Ellipse2D.Double(x, y, size, size);

        g2d.setColor(this.color);
        if (this.fill) {
            g2d.fill(circle);
        } else {
            g2d.draw(circle);
        }
    }
}
