package lab8.ex3;

import java.util.ArrayList;
import java.util.List;

public class Group {
	List<String> students = new ArrayList<String>();
	String name;
	public Group(List<String> students, String name) {
		super();
		this.students = students;
		this.name = name;
	}
	
	public Group (String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		if (name.isBlank()) {
			return name;
		}
		return "Group " + name;
	}
}
