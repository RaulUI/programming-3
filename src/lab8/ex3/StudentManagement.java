package lab8.ex3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;

import lab7.Utils;
import lab8.Song;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.BorderLayout;

public class StudentManagement extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5145069410832023807L;

	DefaultListModel<String> studentsModel = new DefaultListModel<>();
	DefaultListModel<String> assignedModel = new DefaultListModel<>();
	DefaultComboBoxModel<Group> groupsModel = new DefaultComboBoxModel<>();
	JLabel lblAssigned = new JLabel("Assigned Students");
	
	protected static List<String> students = Utils.readStrings("src/lab7/data/student_names", "txt");
	protected static List<Group> groups = new ArrayList<Group>();
	private JTextField tfGroupName;
	
	public static void main(String[] args) {
		new StudentManagement();
	}
	
	public StudentManagement () {
		createGroups(3);
		groupsModel.addAll(groups);
		setSize(900,600);
		getContentPane().setLayout(new GridLayout(0, 3, 0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		studentsModel.addAll(students);
		panel.setLayout(new BorderLayout(0, 0));
		JList lstStudents = new JList(studentsModel);
		panel.add(lstStudents);
		
		JLabel lblNewLabel = new JLabel("Unassigned Students");
		panel.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JComboBox<String> cbGroup = new JComboBox(groupsModel);
		cbGroup.setBounds(74, 11, 113, 22);
		panel_1.add(cbGroup);
		cbGroup.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Group selected = (Group) groupsModel.getSelectedItem();
				lblAssigned.setText(selected + " Assigned Students");
				List<String> assigned = new ArrayList<>();
				List<String> unassigned = new ArrayList<String> ();
				for (String st : students) {
					if (selected.students.contains(st)) {
						assigned.add(st);
					} else {
						unassigned.add(st);
					}
				}
				refreshListData(assignedModel, assigned);
				refreshListData(studentsModel, unassigned);
			}
		});
		
		JButton btnNewButton = new JButton(">");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedStudent = (String) lstStudents.getSelectedValue();
				if (selectedStudent == null) {
					System.out.println("No student was selected");
					return;
				}
				int selectedGroupId = cbGroup.getSelectedIndex();
				if (selectedGroupId == -1) {
					System.out.println("Please select a valid group!");
					return;
				}
				
				//Move the student
				
				//Remove the student from current groups
				for (Group gr : groups) {
					if (gr.students.contains(selectedStudent)) {
						gr.students.remove(selectedStudent);
					}
				}
				//Assign it to the new group
				groups.get(selectedGroupId).students.add(selectedStudent);
				//Update the list models
				assignedModel.add(assignedModel.getSize(), selectedStudent);
				studentsModel.removeElement(selectedStudent);
			}
		});
		btnNewButton.setBounds(104, 142, 50, 23);
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Auto Assign");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int currentGroup = 1;
				//Clear groups before adding students to them
				for (Group gr : groups) {
					gr.students.clear();
				}
				
				//Get the current group and add a student to it 
				for (String student : students) {
					int index = currentGroup++%groups.size();
					if (index == 0) {
						index++;
						currentGroup++;
					}
					if (!groups.get(index).name.isBlank()) {
						groups.get(index).students.add(student);
					}
				}
			}
		});
		btnNewButton_1.setBounds(74, 112, 113, 23);
		panel_1.add(btnNewButton_1);
		
		tfGroupName = new JTextField();
		tfGroupName.setToolTipText("Group Name");
		tfGroupName.setBounds(10, 66, 159, 20);
		panel_1.add(tfGroupName);
		tfGroupName.setColumns(10);
		
		JButton btnNewButton_2 = new JButton("Add Group");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Group group = new Group(tfGroupName.getText());
				groups.add(group);
				groupsModel.addElement(group);
			}
		});
		btnNewButton_2.setBounds(184, 65, 102, 23);
		panel_1.add(btnNewButton_2);
		
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		panel_2.add(lblAssigned, BorderLayout.NORTH);

		JList lstAssigned = new JList(assignedModel);
		panel_2.add(lstAssigned);
		
        setVisible(true);
	}
	
	private static List<Group> createGroups (int nb) {
		groups.clear();
		groups.add(new Group(" "));
		for (int i = 0; i < nb; i++) {
			groups.add(new Group(""+(i+1)));
		}
		return groups;
	}
	
	private <T> void refreshListData (DefaultListModel model, List<T> data) {
		model.clear();
		model.addAll(data);
	}
}
