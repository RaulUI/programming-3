package lab14;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class Lab14 {
	final static String PATH = "src/lab14/";
	final static String NAME = "account.txt";
	public static int balance = getBalance();
	
	public static void main(String[] args) {
		System.out.println(balance);
		Client c1 = new Client(1);
		Client c2 = new Client(2);
		c1.deposit(50);
		c2.deposit(30);
		c2.retrieve(60);
		c1.retrieve(20);
	}
	
	public static int getBalance () {
		String line = "";
		try {
			LineNumberReader lnr = new LineNumberReader(new FileReader(PATH + NAME));
			line = lnr.readLine();
			lnr.close();
			return Integer.parseInt(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}
}
