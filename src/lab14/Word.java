package lab14;

import java.util.List;

public class Word extends Thread {
	String word;
	
	public Word (String word) {
		this.word = word;
		start();
	}

	public void run () {
		System.out.println("Started Word thread for word " + word);
	}
	
	public void checkText (List<String> text) {
		for (String line : text) {
			if (line.contains(word)) {
				System.out.printf("Line %d contains word %s: %s", text.indexOf(line), word, line);
			}
		}
	}
}
