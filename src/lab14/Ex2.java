package lab14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex2 {
	final static String PATH = "src/lab14/";
	final static String TEXT_NAME = "text.txt";
	final static String WORDS_NAME = "words.txt";
	
	static List<String> text = new ArrayList<String>();
	static List<Word> words = new ArrayList<Word>();
	
	public static void main (String ... args) {
		getText();
		checkWords();
	}
	
	public static void getText () {
		try {
			Scanner s = new Scanner(new File(PATH + TEXT_NAME));
			while(s.hasNext()) {
				text.add(s.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(text);
	}
	
	public static void checkWords () {
		try {
			Scanner s = new Scanner(new File(PATH + WORDS_NAME));
			while(s.hasNext()) {
				Word w = new Word(s.nextLine()); 
				words.add(w);
				w.checkText(text);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
