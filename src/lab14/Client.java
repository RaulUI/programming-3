package lab14;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

public class Client extends Thread {
	long id;
	
	public Client (long id) {
		this.id = id;
		this.start();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public void run () {
		System.out.println("Started client " + id);
	}
	
	public void deposit (int amount) {
		try {
			//Delete all from the file
			PrintWriter pw = new PrintWriter(Lab14.PATH + Lab14.NAME);
			pw.close();
			RandomAccessFile raf 
            = new RandomAccessFile(new File(Lab14.PATH + Lab14.NAME), "rw"); 

	        // Write objects to file
			raf.writeBytes(""+(Lab14.balance+amount));
			Lab14.balance += amount;
			System.out.printf("Client %d deposited %d. Current balance is %d\n", id, amount, Lab14.balance);
	        
			raf.close();
	    } catch (IOException e) {
	        System.out.println("Error initializing stream");
	    }
	}
	
	public void retrieve (int amount) {
		try {
			//Delete all from the file
			PrintWriter pw = new PrintWriter(Lab14.PATH + Lab14.NAME);
			pw.close();
			
			RandomAccessFile raf 
            = new RandomAccessFile(new File(Lab14.PATH + Lab14.NAME), "rw"); 

	        // Write objects to file
			raf.writeBytes(""+(Lab14.balance-amount));
			Lab14.balance -= amount;
			System.out.printf("Client %d retrieved %d. Current balance is %d\n", id, amount, Lab14.balance);
	        
			raf.close();
	    } catch (IOException e) {
	        System.out.println("Error initializing stream");
	    }
	}
}
