package lab2;

/**
 * @author raulu
 *
 */
public class Actor {
	public final static String NO_SCHOOL = "NO SCHOOL";
	
	private String name;
	private String lastName;
	protected int yearOfBirth;
	public String actingSchool;	
	
	/**
	 * Empty constructor
	 */
	public Actor() {
		this.actingSchool = NO_SCHOOL;
	}
	
	/**
	 * This will also provide a NO_SCHOOL value to the @param actingSchool variable
	 * @param name
	 * @param lastName
	 */
	public Actor(String name, String lastName) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.actingSchool = NO_SCHOOL;
	}
	

	/**
	 * 
	 * @param name
	 * @param lastName
	 * @param yearOfBirth
	 * @param actingSchool
	 */
	public Actor(String name, String lastName, int yearOfBirth, String actingSchool) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.yearOfBirth = yearOfBirth;
		this.actingSchool = actingSchool;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Override of the toString method
	 */
	@Override
	public String toString() {
		return "Actor [name=" + name + ", lastName=" + lastName + ", yearOfBirth=" + yearOfBirth + ", actingSchool="
				+ actingSchool + "]";
	}
}
