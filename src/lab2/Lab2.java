package lab2;

import java.util.ArrayList;
import java.util.List;

public class Lab2 {
	static boolean any = false;

	public static void main(String[] args) {
		
		Actor2 a1 = new Actor2();
		System.out.println(a1);
		
		List<Actor> actors = new ArrayList<Actor>();		
				
		actors.add(new Actor("Actor", "1", 33, "School 1"));
		actors.add(new Actor("Musician", "2"));
		actors.add(new Actor("Actor", "3", 33, "School 2"));
		actors.add(new Actor("Actor", "4", 33, "School 3"));
		actors.add(new Actor("Actor", "5"));
		actors.add(new Actor("Actor", "6"));

		
//		for (int i = 0; i < actors.size(); i++) {
//			System.out.println(actors.get(i));
//		}
		
		for (Actor actor : actors) {
			System.out.println(actor);
		}
		
//		actors.forEach(actor -> System.out.println(actor));
		
//		long actorNb = actors
//				.stream()
//				.filter(actor -> actor.actingSchool == Actor.NO_SCHOOL)
//				.count();

		int count = 0;
		for (Actor actor : actors) {
			if (actor.actingSchool == Actor.NO_SCHOOL) {
				count++;
			}
		}
		
		System.out.println("Actors number with NO acting school: " + count);
		
		String check = args[0];
		System.out.printf("Actors name that contains or starts with %s are ", check);

		for (Actor actor : actors) {
			if (actor.getName().startsWith(check)) {
				System.out.printf("%s %s, ", actor.getName(), actor.getLastName());
				any = true;
			}
		}
		
		if (!any) {
			System.out.println("none");
		}
	}
}
