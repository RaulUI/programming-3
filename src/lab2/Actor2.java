package lab2;

public class Actor2 {
	private String name; //Camel Case
	private String lastName;
	protected int yearOfBirth;
	public String actingSchool;
	
	public final static String NO_SCHOOL = "NO SCHOOL";

	public Actor2() {
		super();
	}
	
	public Actor2(String name, String lastName) {
		super();
		this.name = name;
		this.lastName = lastName;
	}

	public Actor2(String name, String lastName, int yearOfBirth, String actingSchool) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.yearOfBirth = yearOfBirth;
		this.actingSchool = actingSchool;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getYearOfBirth() {
		return yearOfBirth;
	}

	public void setYearOfBirth(int yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	@Override
	public String toString() {
		return "Actor2 [name=" + name + ", lastName=" + lastName + ", yearOfBirth=" + yearOfBirth + ", actingSchool="
				+ actingSchool + "]";
	}
}
