package lab4;

public class Airplane {
	private String producer, code;
	private int flightCount, fuelCapacity;
	
	public Airplane() {}
	
	public Airplane(String producer, String code, int flightCount, int fuelCapacity) {
		super();
		this.producer = producer;
		this.code = code;
		this.flightCount = flightCount;
		this.fuelCapacity = fuelCapacity;
	}
	
	public String getProducer() {
		return producer;
	}
	public void setProducer(String producer) {
		this.producer = producer;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getFlightCount() {
		return flightCount;
	}
	public void setFlightCount(int flightCount) {
		this.flightCount = flightCount;
	}
	public int getFuelCapacity() {
		return fuelCapacity;
	}
	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}
	@Override
	public String toString() {
		return "Airplane [producer=" + producer + ", code=" + code + ", flightCount=" + flightCount + ", fuelCapacity="
				+ fuelCapacity + "]";
	}
}
