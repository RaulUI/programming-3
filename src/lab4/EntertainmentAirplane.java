package lab4;

import java.util.ArrayList;
import java.util.List;

public class EntertainmentAirplane extends Airplane implements LuxuryOptions {
	protected String owner;
	protected List<String> previousOwners = new ArrayList<String>();
	
	public EntertainmentAirplane(String producer, String code, int flightCount, int fuelCapacity, String owner,
			List<String> previousOwners) {
		super(producer, code, flightCount, fuelCapacity);
		this.owner = owner;
		this.previousOwners = previousOwners;
	}
	
	@Override
	public boolean noiseCancelling() {
		return true;
	}
	
	@Override
	public boolean touchScreen() {
		return true;
	}
}
