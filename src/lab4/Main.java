package lab4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		
		//Creation of the airplanes list
		List<Airplane> airplanes = new ArrayList<Airplane>();
		airplanes.add(new FighterAirplane("Producer 1", "Code 1", 1000, 100, true, 132445));
		airplanes.add(new FighterAirplane("Producer 2", "Code 2", 2000, 200, false, 2153));
		airplanes.add(new LineAirplane("Producer 3", "Code 3", 3000, 300, 265));
		airplanes.add(new LineAirplane("Producer 4", "Code 4", 4000, 400, 376));
		airplanes.add(new LineAirplane("Producer 5", "Code 5", 5000, 500, 152));
		airplanes.add(new LineAirplane("Producer 6", "Code 6", 6000, 600, 30));
		airplanes.add(new EntertainmentAirplane("Producer 7", "Code 7", 7000, 700, "Owner 1", Arrays.asList("POwner 1", "POwner 2")));
		airplanes.add(new EntertainmentAirplane("Producer 8", "Code 8", 8000, 800, "Owner 2", Arrays.asList("POwner 3", "POwner 4", "POwner 5")));

		System.out.println("2)" + airplanes);
		
		//Declaration of the map
		Map<String, String> planeCounts = new HashMap<String, String>();
		
		//planeCounts.put(key, value); //Adds a pair (key, value) to the map
		//planeCounts.get(key); //Return the value at the specific key
		//System.out.println(planeCounts); //Display the map
		
		//Initialization of the Map
		List<String> planeTypes = Arrays.asList("FighterAirplane", "LineAirplane", "EntertainmentAirplane");
		for (String type : planeTypes) {
			planeCounts.put(type, "");
		}
		
		//Counting the airplanes
		for (Airplane airplane : airplanes) {
			String planeType = airplane.getClass().getSimpleName();
			planeCounts.put(planeType, planeCounts.get(planeType) + "*");
		}
		
		System.out.println("3)" + planeCounts);
		
		System.out.println("4)");
		for (Airplane airplane : airplanes) {
			if (airplane instanceof LuxuryOptions && ((LuxuryOptions)airplane).noiseCancelling()) {
				System.out.println(airplane);
			}
		}
		
		System.out.println("5)" + Collections
				.max(airplanes, 
						Comparator.comparing(plane -> 
						plane instanceof EntertainmentAirplane ? ((EntertainmentAirplane)plane).previousOwners.size() : 0)));
		
		System.out.printf("6) Average of all airplanes %s average of fighters is %s\n",
				airplanes.stream()
	                .map(Airplane::getFuelCapacity)
	                .mapToDouble(a -> a)
	                .average()
	                .getAsDouble(),
	                
                airplanes
                	.stream()
                	.filter(plane -> plane instanceof FighterAirplane)
	                .map(Airplane::getFuelCapacity)
	                .mapToDouble(a -> a)
	                .average()
	                .getAsDouble()
            );
	}
	
	public Airplane getMax (List<Airplane> planes) {
		Airplane maxPlane = new Airplane();
		int maximum = 0;
		for (Airplane plane : planes) {
			if (plane instanceof EntertainmentAirplane) {
				int currentCount = ((EntertainmentAirplane)plane).previousOwners.size();
				if (currentCount > maximum) {
					maximum = currentCount;
					maxPlane = plane;
				}
			}
		}
		return maxPlane;
	}
}
