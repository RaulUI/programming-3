package lab4;

public interface LuxuryOptions {
	public boolean noiseCancelling();
	public boolean touchScreen();
}
