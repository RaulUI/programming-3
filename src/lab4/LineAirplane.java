package lab4;

public class LineAirplane extends Airplane implements LuxuryOptions {
	protected int capacity;

	public LineAirplane(String producer, String code, int flightCount, int fuelCapacity, int capacity) {
		super(producer, code, flightCount, fuelCapacity);
		this.capacity = capacity;
	}

	@Override
	public boolean noiseCancelling() {
		return true;
	}

	@Override
	public boolean touchScreen() {
		return false;
	}
}
