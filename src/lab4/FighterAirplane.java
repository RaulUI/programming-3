package lab4;

public class FighterAirplane extends Airplane {
	protected boolean camouflage;
	protected int weaponCapacity;
	
	public FighterAirplane(String producer, String code, int flightCount, int fuelCapacity, boolean camouflage,
			int weaponCapacity) {
		super(producer, code, flightCount, fuelCapacity);
		this.camouflage = camouflage;
		this.weaponCapacity = weaponCapacity;
	}
}
