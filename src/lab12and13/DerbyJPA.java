package lab12and13;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class DerbyJPA {
	public static EntityManagerFactory factory = Persistence.createEntityManagerFactory("jpaExample");
	@PersistenceContext
	public static EntityManager em = factory.createEntityManager();
	
	@SuppressWarnings("unchecked")
	public static List<Receipt> getReceipts() {
    	return em.createQuery("SELECT p FROM Receipt p").getResultList();
    }
	
	public static void addReceipt (Receipt receipt) {
		Receipt old = em.find(Receipt.class, receipt.getId());
		if (old == null) {
			em.getTransaction().begin();
			em.persist(receipt);
			em.getTransaction().commit();
		}
	}
	
	public static void updateReceipt (Receipt receipt) {
		Receipt old = em.find(Receipt.class, receipt.getId());
		
		em.getTransaction().begin();
		
		old.setAmount(receipt.amount);
		old.setCode(receipt.code);
		old.setClient(receipt.client);
		old.setContractor(receipt.contractor);
		old.setDate(receipt.date);
		
		em.getTransaction().commit();
	}
	
	public static int getPaidReceipts (String contractor) {
		TypedQuery<Receipt> q = em.createQuery("SELECT p FROM Receipt p WHERE p.contractor = :contractor", Receipt.class);
    	q.setParameter("contractor", contractor);
		List<Receipt> receipts = q.getResultList();
		return receipts.stream().mapToInt(Receipt::getAmount).sum();
	}
}
