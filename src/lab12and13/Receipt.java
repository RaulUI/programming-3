package lab12and13;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Receipt {
	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	long id;
	
	@Column(name="code")
	int code;
	
	@Column
	String client;
	
	@Column
	String contractor;
	
	@Column
	int amount;
	
	@Column
	Date date;
	
	public Receipt(long id, int code, String client, String contractor, int amount, Date date) {
		super();
		this.id = id;
		this.code = code;
		this.client = client;
		this.contractor = contractor;
		this.amount = amount;
		this.date = date;
	}
	
	public Receipt(int code, String client, String contractor, int amount, Date date) {
		super();
		this.code = code;
		this.client = client;
		this.contractor = contractor;
		this.amount = amount;
		this.date = date;
	}
	
	public Receipt() {
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getContractor() {
		return contractor;
	}
	public void setContractor(String contractor) {
		this.contractor = contractor;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "Receipt [id=" + id + ", code=" + code + ", client=" + client + ", contractor=" + contractor
				+ ", amount=" + amount + ", date=" + date + "]";
	}
}
