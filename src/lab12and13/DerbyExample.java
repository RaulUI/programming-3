package lab12and13;

import java.sql.Date;

public class DerbyExample {
	public final static String TABLE_NAME = "Receipt";

	public static void main(String[] args) {
//		DerbyDB derby = new DerbyDB(TABLE_NAME);
//		derby.createTableIfNotExistent();
//		derby.add(new Receipt(1, 123, "Client 1", "Accountant 1", 100, new Date(10,10,2020)));
//		System.out.println(derby.read());
		
		DerbyJPA.addReceipt(new Receipt(123, "Client 3", "Accountant 3", 300, new Date(0,10,20)));
		System.out.println(DerbyJPA.getReceipts());
		System.out.println(DerbyJPA.getPaidReceipts("Accountant 1"));
	}

}
