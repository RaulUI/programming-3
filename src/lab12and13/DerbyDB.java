package lab12and13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DerbyDB implements DAO {
	Connection conn;
	String tableName;
	
	public DerbyDB (String tableName) {
		this.tableName = tableName;
		connect();
	}
	
	@Override
	public void connect() {
		String dbURL = "jdbc:derby://localhost:1527/receipt;create=true";
		try {
			conn = DriverManager.getConnection(dbURL);
		} catch (SQLException e) {
			System.err.println("Error trying to connect to the database.");
			System.exit(0);
		}
	}
	
	@Override
	public void createTableIfNotExistent() {
		Statement stmt = null;
		//Creating the Statement object
		try {
			stmt  = conn.createStatement();
		} catch (SQLException e1) {
			System.err.println("Failed creating a statement.");
		}
		
	    try {
			stmt.execute("SELECT * FROM " + tableName + " FETCH FIRST ROW ONLY");
		} catch (SQLException e) {
			//Code 42X05 just tells us there is no table available
			if (!e.getSQLState().equals("42X05")) {
				e.printStackTrace();
				return;
			}
		}
	 
		//Executing the query
	    String query = "CREATE TABLE " + tableName + "( "
	         + "id INT NOT NULL GENERATED ALWAYS AS IDENTITY(Start with 1, Increment by 1), "
	         + "code INT NOT NULL, "
	         + "client VARCHAR(50), "
	         + "contractor VARCHAR(50), "
	         + "amount INT, "
	         + "date DATE, "
	         + "PRIMARY KEY (id))";
//	    query = "DROP TABLE "+ tableName;
	    try {
			stmt.execute(query);
		} catch (SQLException e) {
			//Code X0Y32 tells us there already is a table with that name
			if (e.getSQLState().equals("X0Y32")) {
				System.out.println("Table " + tableName + " already exists in the DB.");
			} else {
				System.err.println("Failed creating the table");
			}
			return;
		}
        System.out.println("Table created");
	}

	@Override
	public <T> void update(T obj) {
		Receipt r = (Receipt)obj;
		String query = "UPDATE " + tableName 
				+ " SET \"code\"=?, "
				+ "\"client\"=?, "
				+ "contractor=?, "
				+ "amount=?, "
				+ "\"date\"=? "
				+ "WHERE id=?";
		try {
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, r.code);
			stmt.setString(2, r.client);
			stmt.setString(3, r.contractor);
			stmt.setInt(4, r.amount);
			stmt.setDate(5, r.date);
			stmt.setLong(6, r.id);
			stmt.execute();
		} catch (SQLException e) {
			System.out.println("Could not update data for " + obj);
		}
	}

	@Override
	public <T> void add(T obj) {
		Receipt receipt = (Receipt)obj;
		String query = "INSERT INTO " + tableName + "(code, client, contractor, amount, date) VALUES (?, ?, ?, ?, ?)";
		try {
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, receipt.code);
			stmt.setString(2, receipt.client);
			stmt.setString(3, receipt.contractor);
			stmt.setInt(4, receipt.amount);
			stmt.setDate(5, receipt.date);
			stmt.execute();
		} catch (SQLException e) {
			System.out.println("Could not add data " + obj);
		}
	}

	@Override
	public void delete(int id) {
		
	}

	@Override
	public <T> List<T> read() {
		Statement stmt;
		List<Receipt> objs = new ArrayList();
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
			while(rs.next()) {
				Receipt r = new Receipt();
				r.setId(rs.getLong("id"));
				r.setCode(rs.getInt("code"));
				r.setClient(rs.getString("client"));
				r.setContractor(rs.getString("contractor"));
				r.setAmount(rs.getInt("amount"));
				r.setDate(rs.getDate("date"));
				
				objs.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return (List<T>) objs;
	}
}
