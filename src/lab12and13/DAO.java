package lab12and13;

import java.util.List;

public interface DAO {
	void connect();
	void createTableIfNotExistent();
	<T> void update(T obj);
	<T> void add(T obj);
	void delete(int id);
	<T> List<T> read();
}
