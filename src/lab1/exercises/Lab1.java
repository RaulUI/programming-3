package lab1.exercises;

public class Lab1 {

	public static void main(String [] args) {
		Exercises ex = new Exercises ();
		System.out.println(ex);
		
		ex.helloWorld();
		ex.helloArgs(args);
		ex.scd(5,25);
		
		int n = 0;
		try {
			n = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.err.println("First argument passed is not a number.");
			return;
		} catch (Exception e) {
			System.err.println("An exception occured when tried to parse first argument.");
			return;
		}
		
		ex.diceThrow(n);
		ex.array(n);
	}
}
