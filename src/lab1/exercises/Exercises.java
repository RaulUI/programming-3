package lab1.exercises;

import java.util.Arrays;
import java.util.Random;

public class Exercises {
	
	public void helloWorld () {
		System.out.println("Hello world!");
	}
	
	public void helloArgs (String ... args) {
		System.out.println("Hello " + Arrays.asList(args));
	}
	
	public void scd (int a, int b) {
		while (a != b) {
			if (a > b) {
				a -= b;
			} else {
				b -= a;
			}
		}
		System.out.println("Smallest common divisor is " + a);
	}
	
	public void diceThrow (int n) {
		Random rand = new Random();
		
		for (int i = 0; i < n; i++) {
			System.out.print((rand.nextInt(6)+1) + " ");
		}
		System.out.println();
	}

	public void array(int n) {
		Random rand = new Random();
		Integer [] a = new Integer[n];
		
		for (int i = 0; i < n; i++) {
			a[i] = rand.nextInt(6);
		}
		
		// a)
		System.out.println("Random generated array: " + Arrays.asList(a));	
		
		// b)
		Arrays.sort(a);
		System.out.println("Sorted array: " + Arrays.asList(a));
		
		// c)
		int x = 3;
		System.out.println("Does the list contain " + x + "? " + Arrays.asList(a).contains(x));
		
		// d)
		int start = rand.nextInt(n);
		int end = rand.nextInt(n-start) + start;
		Integer [] b = Arrays.copyOfRange(a, start, end);
		System.out.println("Sublist created from index " + start + " to index " + end + " is " + Arrays.asList(b));
	}
	
	public String toString () {
		return "Starting to show exercises from the first lab!";
	}
}
